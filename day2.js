function valueElement(a, n) {
  const arr = [];
  for (let i = n - 1; i < a.length; i += n) {
    arr.push(a[i]);
  }
  return arr;
}

const a = [1, 2, 3, 4, 5, 6, 7, 8];
const myRest = valueElement(a, 4);
// console.log(myRest)

// assg 1
function pickerText(arr, i) {
  if (i < 0 || i >= arr.length) {
    return "throw error: Invalid weekday number";
  } else {
    const ChoseText = arr[i];
    return ChoseText;
  }
}

const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
//   console.log(pickerText(weekdays, 0));
//   console.log(pickerText(weekdays, 3));
//   console.log(pickerText(weekdays, 5));

//asg 2
function birthday(s, d, m) {
  let sum = 0,
    c = 0;
  for (let i = 0; i <= s.length - m; ++i) {
    sum += s[i];
    for (let j = i + m; j-- > i + 1; ) {
      sum += s[j];
    }
    if (sum === d) ++c;
    sum = 0;
  }

  return c;
}

console.log(birthday([2, 2, 1, 3, 2], 4, 2));
console.log(birthday([1, 2, 1, 3, 2], 3, 2));
console.log(birthday([1, 1, 1, 1, 1], 3, 2));
console.log(birthday([4], 4, 1));
