import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const Headers = ({title, search, navigation, onPress}) => {
  return (
    <View style={styles.header}>
      <View style={styles.back}>
        <View>
          <TouchableOpacity onPress={onPress}>
            <Image
              source={{
                uri: 'https://img.icons8.com/external-royyan-wijaya-detailed-outline-royyan-wijaya/512/external-arrow-arrow-line-royyan-wijaya-detailed-outline-royyan-wijaya-5.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.text}>{title}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginBottom: 3,
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
});

export default Headers;
