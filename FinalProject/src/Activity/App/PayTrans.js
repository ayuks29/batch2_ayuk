import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {RadioButton} from 'react-native-paper';
import Headers from '../../Component/Headers';
import {useDispatch} from 'react-redux';
import {PayTransaction} from '../../Redux/Action/Action';
import icTransaction from '../../icons/transaction.png';

const PayTrans = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [disables, setDisables] = useState(false);
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');
  const dispatch = useDispatch();

  const ValidatorButton = () => {
    return dataAmount !== '' &&
      dataSender !== '' &&
      dataTarget !== '' &&
      dataType !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [dataAmount, dataSender, dataTarget, dataType]);

  const showToast = () => {
    ToastAndroid.show('Congrats '+dataSender+', You send'+' Rp' + dataAmount+ ' to ' +dataTarget, ToastAndroid.SHORT);
  };

  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  async function pay(e) {
    try {
      dispatch(PayTransaction(dataAmount, dataSender, dataTarget, dataType));
      console.log('Transaksi Berhasil');
    } catch (e) {
      console.log('error', e);
    }
  }

  const handlePay = () => {
    pay();
    navigation.navigate('History');
    showToast();
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{backgroundColor: 'white', flex: 1}}>
      <SafeAreaView>
        <ScrollView>
          <View>
            <Headers
              title={'PayTrans'}
              navigation={navigation}
              onPress={() => {
                navigation.goBack();
              }}
            />
            <View>
              <Image
                source={icTransaction}
                style={{
                  height: 170,
                  width: 200,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                  marginTop: 10,
                }}
              />
            </View>
            <View
              style={{
                borderWidth: 1,
                margin: 20,
                borderColor: '#d9d9d9',
                borderRadius: 8,
                backgroundColor: '#e2e6e9',
                padding: 10,
              }}>
              <Text
                style={{
                  marginTop: 5,
                  marginLeft: 20,
                  color: 'black',
                  fontFamily: 'Questrial-Regular',
                }}>
                Type of Transaction
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  margin: 3,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <RadioButton
                    value="kontak"
                    status={dataType === 'kontak' ? 'checked' : 'unchecked'}
                    onPress={() => setDataType('kontak')}
                    color="#0F5E38"
                  />
                  <Text
                    style={{
                      fontFamily: 'Questrial-Regular',
                    }}>
                    Send to Contact
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <RadioButton
                    value="bank"
                    status={dataType === 'bank' ? 'checked' : 'unchecked'}
                    onPress={() => setDataType('bank')}
                    color="#0F5E38"
                  />
                  <Text
                    style={{
                      fontFamily: 'Questrial-Regular',
                    }}>
                    Send to Bank
                  </Text>
                </View>
              </View>
              <TextInput
                style={styles.txt}
                placeholder="amount"
                placeholderTextColor={'black'}
                keyboardType={'number-pad'}
                value={dataAmount}
                onChangeText={am => {
                  setDataAmount(rupiah(am || ''));
                }}
              />

              <TextInput
                style={styles.txt}
                placeholder="sender"
                placeholderTextColor={'black'}
                value={dataSender}
                onChangeText={send => {
                  setDataSender(send);
                }}
              />

              <TextInput
                style={styles.txt}
                placeholder="receiver"
                placeholderTextColor={'black'}
                value={dataTarget}
                onChangeText={tg => {
                  setDataTarget(tg);
                }}
              />
            </View>

            <View>
              <TouchableOpacity
                disabled={disables}
                onPress={() => {
                  handlePay();
                }}
                style={{
                  backgroundColor: disables == false ? '#0F5E38' : '#f0f3f4',
                  margin:20,
                  padding:10,
                  borderRadius: 8,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: disables == false ? 'white' : 'black',
                    fontSize: 17,
                    fontFamily: 'Questrial-Regular',
                  }}>
                  Send
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  txt: {
    height: 50,
    borderWidth: 1,
    borderRadius: 8,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    backgroundColor: 'white',
    borderColor:'#0F5E38',
    alignItems: 'center',
    marginTop: 5,
    fontFamily: 'Questrial-Regular',
    marginBottom: 5,
  },
});

export default PayTrans;
