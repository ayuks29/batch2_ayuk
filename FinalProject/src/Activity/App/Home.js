import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
} from 'react-native';
import React from 'react';
import icSaldo from '../../icons/saldo.png';
import icData from '../../icons/paydata.png';
import icPulsa from '../../icons/paypulsa.png';
import icTran from '../../icons/paytran.png';
import icVoc from '../../icons/voucher.png';
import icGame from '../../icons/game.png';
import icEmo from '../../icons/emoney.png';
import icHist from '../../icons/history.png';
import icMore from '../../icons/more.png';
import {useDispatch} from 'react-redux';
import {Logout} from '../../Redux/Action/Action';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Home = ({navigation}) => {
  //REDUX
  const dispatch = useDispatch();
  async function logout() {
    try {
      console.log('logout');
      dispatch(Logout());
      await AsyncStorage.removeItem('Token');
    } catch (e) {
      console.log('error', e);
    }
  }
  const showToast = () => {
    ToastAndroid.show('You are Successfully LoggedOut', ToastAndroid.SHORT);
  };
  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          alignContent: 'center',
          marginTop: -5,
        }}>
        <Text
          style={{
            fontFamily: 'CarterOne-Regular',
            fontSize: 28,
            color: '#0f5e38',
            marginBottom: 10,
          }}>
          Paya
        </Text>
        <View
          onTouchEnd={async () => {
            logout();
            showToast();
          }}>
          <Image
            source={{
              uri: 'https://img.icons8.com/fluency-systems-filled/512/exit.png',
            }}
            style={{
              height: 28,
              width: 28,
            }}
          />
        </View>
      </View>
      {/* SALDO */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          borderWidth: 1,
          borderRadius: 10,
          padding: 12,
          borderColor: '#d9d9d9',
        }}>
        <View
          style={{
            flexDirection: 'column',
          }}>
          <Text
            style={{
              fontFamily: 'Questrial-Regular',
              fontSize: 14,
              color: 'black',
            }}>
            Paya Saldo
          </Text>
          <Text
            style={{
              fontFamily: 'Questrial-Regular',
              fontSize: 20,
              color: 'black',
              marginTop: 10,
            }}>
            Rp 500.0000
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <Image
            source={icSaldo}
            style={{
              height: 28,
              width: 30,
              resizeMode: 'contain',
              marginTop: 5,
            }}
          />
          <Text
            style={{
              fontFamily: 'Questrial-Regular',
              color: '#0F5E38',
            }}>
            TopUp
          </Text>
        </View>
      </View>
      {/* PAYMENT */}
      <View
        style={{
          marginTop: 15,
        }}>
        <Text
          style={{
            fontFamily: 'Questrial-Regular',
            color: 'black',
            fontSize: 18,
          }}>
          Payment
        </Text>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            marginLeft: 15,
            marginRight: 15,
            justifyContent: 'space-between',
          }}>
          {/* PAYMENT 1 */}
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                width: 50,
                height: 50,
                backgroundColor: '#e6e6e6',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: '#e6e6e6',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={icData}
                style={{
                  height: 38,
                  width: 38,
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 13,
              }}>
              PayData
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                width: 50,
                height: 50,
                backgroundColor: '#e6e6e6',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: '#e6e6e6',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={icPulsa}
                style={{
                  height: 38,
                  width: 38,
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 13,
              }}>
              PayPulsa
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('PayTrans');
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: '#e6e6e6',
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: '#e6e6e6',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={icTran}
                  style={{
                    height: 38,
                    width: 38,
                  }}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 13,
              }}>
              PayTrans
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                width: 50,
                height: 50,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: '#e6e6e6',
                backgroundColor: '#e6e6e6',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={icVoc}
                style={{
                  height: 38,
                  width: 38,
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 13,
              }}>
              Vocher
            </Text>
          </View>
        </View>

        {/* PAYMENY 2 */}
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            marginLeft: 15,
            marginRight: 15,
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                width: 50,
                height: 50,
                backgroundColor: '#e6e6e6',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: '#e6e6e6',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={icGame}
                style={{
                  height: 38,
                  width: 38,
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 13,
              }}>
              Games
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                width: 50,
                height: 50,
                backgroundColor: '#e6e6e6',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: '#e6e6e6',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={icEmo}
                style={{
                  height: 38,
                  width: 38,
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 13,
              }}>
              E-Money
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('History');
              }}>
              <View
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: '#e6e6e6',
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: '#e6e6e6',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={icHist}
                  style={{
                    height: 38,
                    width: 38,
                  }}
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 13,
              }}>
              History
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                width: 50,
                height: 50,
                borderRadius: 5,
                borderWidth: 1,
                borderColor: '#e6e6e6',
                backgroundColor: '#e6e6e6',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                source={icMore}
                style={{
                  height: 38,
                  width: 38,
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                fontSize: 13,
              }}>
              More
            </Text>
          </View>
        </View>
        {/* PROMO&DISCOUNT */}
        <View
          style={{
            marginTop: 15,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              alignContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                color: 'black',
                fontSize: 18,
              }}>
              Promo and Discount
            </Text>
            <Text
              style={{
                fontFamily: 'Questrial-Regular',
                color: '#0F5E38',
                fontSize: 15,
              }}>
              See More
            </Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
          }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View
              style={{
                height: 230,
                width: 373,
                backgroundColor: '#0F5E38',
                marginTop: 8,
                borderRadius: 8,
                marginRight: 10,
                padding: 20,
                flexDirection: 'row',
              }}>
              <View>
                <Text
                  style={{
                    fontFamily: 'Questrial-Regular',
                    color: 'white',
                    fontSize: 18,
                    marginTop: 30,
                  }}>
                  Top-up E-Money
                </Text>
                <Text
                  style={{
                    fontFamily: 'Questrial-Regular',
                    color: 'white',
                    fontSize: 18,
                  }}>
                  discount
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Questrial-Regular',
                      color: 'white',
                      fontSize: 13,
                    }}>
                    up to Rp.
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Questrial-Regular',
                      color: 'white',
                      fontSize: 40,
                    }}>
                    15.000
                  </Text>
                </View>
              </View>
              <View
                style={{
                  marginLeft: 20,
                  justifyContent: 'center',
                }}>
                <Image
                  source={{
                    uri: 'https://cdn-icons-png.flaticon.com/512/8730/8730741.png',
                  }}
                  style={{
                    width: 150,
                    height: 150,
                  }}
                />
              </View>
            </View>
            {/*  */}
            <View
              style={{
                height: 230,
                width: 373,
                backgroundColor: '#0F5E38',
                marginTop: 8,
                // marginLeft:10,
                borderRadius: 8,
                padding: 20,
                flexDirection: 'row',
              }}>
              <View>
                <Text
                  style={{
                    fontFamily: 'Questrial-Regular',
                    color: 'white',
                    fontSize: 18,
                    marginTop: 30,
                  }}>
                  Top-up E-Money
                </Text>
                <Text
                  style={{
                    fontFamily: 'Questrial-Regular',
                    color: 'white',
                    fontSize: 18,
                  }}>
                  discount
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Questrial-Regular',
                      color: 'white',
                      fontSize: 13,
                    }}>
                    up to Rp.
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Questrial-Regular',
                      color: 'white',
                      fontSize: 40,
                    }}>
                    15.000
                  </Text>
                </View>
              </View>
              <View
                style={{
                  marginLeft: 20,
                  justifyContent: 'center',
                }}>
                <Image
                  source={{
                    uri: 'https://cdn-icons-png.flaticon.com/512/8730/8730741.png',
                  }}
                  style={{
                    width: 150,
                    height: 150,
                  }}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    margin: 18,
  },
});

export default Home;
