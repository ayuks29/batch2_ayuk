import {View, Text} from 'react-native';
import React, { useEffect } from 'react';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    const timeout = setTimeout(() => {
      navigation.replace('Login');
    }, 1500);
    return () => clearTimeout(timeout);
  }, []);

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        backgroundColor: 'white',
      }}>
      <Text
        style={{
          fontFamily: 'CarterOne-Regular',
          fontSize: 35,
          color: '#0f5e38',
        }}>
        Paya
      </Text>
      <Text
        style={{
          fontFamily: 'BalooBhai2-VariableFont_wght',
          fontSize: 17,
        }}>
        The Best payment
      </Text>
    </View>
  );
};

export default SplashScreen;
