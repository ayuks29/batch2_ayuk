import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const Loginn = (email, password) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };

  return dispatch => {
    const data = {
      email,
      password,
    };
    dispatch({
      type: 'LOGIN',
      data: data,
    });
    dataToken(JSON.stringify(data));
  };
};

export const Regist = (dataText, dataEmail, dataPass) => {
  return async dispatch => {
    var data = JSON.stringify({
      name: dataText,
      email: dataEmail,
      password: dataPass,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data), 'Berhasil Hore');
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'REGISTER',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Hist = () => {
  return async dispatch => {
    var data = '';

    var config = {
      method: 'get',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {},
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data, 'ini respon history'));
        const data = response.data;
        dispatch({
          type: 'GET_HISTORY',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });

    return true;
  };
};

export const PayTransaction = (dataAmount, dataSender, dataTarget, dataType) => {
  return async dispatch => {
    var data = JSON.stringify({
      amount: dataAmount,
      sender: dataSender,
      target: dataTarget,
      type : dataType
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data), 'Berhasil Hore');
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'TRANSAKSI',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Logout = () => {
  return dispatch => {
    const data = '';
    dispatch({
      type: 'SIGN_OUT',
      data: data,
    });
  };
};
