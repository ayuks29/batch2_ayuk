const initialState = {
  DataStore: [],
  History: [],
  Transaksi: [],
};

const Fetch = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_DATA':
      return {
        ...state,
        DataStore: action.data,
      };
    case 'GET_HISTORY':
      return {
        ...state,
        History: action.data,
      };
    case 'TRANSAKSI':
      return {
        ...state,
        Transaksi: action.data,
      };
    default:
      return state;
  }
};

export default Fetch;
