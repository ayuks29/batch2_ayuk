import {combineReducers} from 'redux';
import Auth from './Auth';
import Fetch from './Fetch';

export default combineReducers({
  Auth,
  Fetch,
});
