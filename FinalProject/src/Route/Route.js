import React, {useEffect, useState} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationContainer} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import SplashScreen from '../Activity/Auth/SplashScreen';
import Login from '../Activity/Auth/Login';
import Register from '../Activity/Auth/Register';
import Home from '../Activity/App/Home';
import PayTrans from '../Activity/App/PayTrans';
import History from '../Activity/App/History';
const Stack = createNativeStackNavigator();

const AppStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="PayTrans" component={PayTrans} />
      <Stack.Screen name="History" component={History} />
    </Stack.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="SplashScreen" component={SplashScreen} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
    </Stack.Navigator>
  );
};


function Route() {
  const [tokens, settokens] = useState(null);
  const UpdateData = useSelector(state => state.Auth.AuthData);
  const DataUser = async () => {
    const token = await AsyncStorage.getItem('Token');
    settokens(token);
  };
  useEffect(() => {
    DataUser();
  }, [UpdateData]);

  return (
    <NavigationContainer>
      {/* <Splash/> */}
      {tokens !== null ? (
        <AppStack />
      ) : (
        <AuthStack />
      )}
    </NavigationContainer>
  );
}

export default Route;
