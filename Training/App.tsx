import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView
} from 'react-native';
import React, {useState} from 'react';
import TextInputs from './TextInputs';


const App = () => {
  const [dataText, setDataText] = useState('');
  const [dataEmail, setdataEmail] = useState('');
  const [dataPass, setdataPass] = useState('')

  function daftar() {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      name: dataText,
      email: dataEmail,
      password : dataPass,
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users/-NMbFuuYUdEHRd9JK82u.json", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result, 'hasil'))
      .catch(error => console.log('error', error));
  }

  console.log(dataText);
  return (
    <KeyboardAvoidingView 
      behavior={Platform.OS === 'ios' ? 'padding' : undefined} 
      style={{ flex: 1 }}
      >
      <SafeAreaView style={style.container}> 
      {/* bisa dipakai bisa engga */}
        <ScrollView> 
        <View>
          <View>
          <Image
            source={{ uri: 'https://cdn-icons-png.flaticon.com/512/8662/8662284.png' }}
            style={{ width: 200, height: 200, marginLeft:20, marginTop:20}}
          />
          </View>

          <View>
            <Text style={{
              marginTop:10,
              marginBottom:20,
              textAlign:'right',
              marginRight:30,
              fontWeight:'bold',
              color:'black',
              fontSize: 18
            }}>
              Create Your Account
            </Text>
          </View>

          <View style={{marginVertical:5}}>
            <TextInputs
                placeholders={"name"}
                values={dataText}
                onChangeTexts={(text: any) => {
                  setDataText(text);
                } } heights={undefined}         />
          </View>

          <View style={{marginVertical:5}}>
            <TextInputs
                placeholders={"email"}
                values={dataEmail}
                onChangeTexts={(text: any) => {
                  setdataEmail(text);
                } } heights={undefined}          />
          </View>

          <View style={{marginVertical:5}}>
            <View
              style={{
                height: 50,
                borderWidth: 1,
                borderRadius: 8,
                marginHorizontal: 20,
                paddingHorizontal: 10,
                backgroundColor:'white',
              }}>
              <TextInput
                placeholder="password"
                placeholderTextColor={'black'}
                value={dataPass}
                onChangeText={text => {
                  setdataPass(text);
                }}
                secureTextEntry={true}
              />
            </View>
          </View>
          
          <TouchableOpacity
            onPress={() => {
              daftar();
            }}
            style={{
              marginHorizontal: 20,
              paddingVertical: 15,
              paddingHorizontal: 20,
              marginTop: 35,
              borderRadius: 8,
              backgroundColor: '#99e6e6',
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: 'black',
                fontSize: 18,
                fontWeight: 'bold',
              }}>
              Register
            </Text>
          </TouchableOpacity>
        </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#ebfafa',
    
  },
});

export default App;