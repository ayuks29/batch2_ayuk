import {View, TextInput, StyleSheet} from 'react-native';
import React, {useState} from 'react';

const textInput = ({placeholders, onChangeTexts, values, heights}) => {
  const [lokalText, setLokalText] = useState('');
  return (
    <View>
      <View
        style={{
          height: 50,
          borderWidth: 1,
          borderRadius: 8,
          marginHorizontal: 20,
          paddingHorizontal: 10,
          justifyContent:'center',
          backgroundColor:'white',
        }}>
        <TextInput
          placeholder={placeholders}
          placeholderTextColor={'black'}
          value={values}
          onChangeText={lokalText => {
            setLokalText(lokalText);
            onChangeTexts(lokalText);
          }}
        />
      </View>
    </View>
  );
};

const style = StyleSheet;

export default textInput;
