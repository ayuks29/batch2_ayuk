import React from 'react'
import Route from './src/route/Route'
import { NavigationContainer } from '@react-navigation/native'

const App = () => {
  return (
    <NavigationContainer>
      <Route/>
    </NavigationContainer>
    
  )
}

export default App