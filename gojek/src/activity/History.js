import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import Headerss from '../component/Headerss';
import React, {useState, useEffect} from 'react';
import icHistpay from '../../icons/historypay.png';

const History = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  useEffect(() => {
    getData();
  }, []);

  const [result, setResult] = useState([]);
  const getData = async () => {
    var raw = '';

    var requestOptions = {
      method: 'GET',
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(response => {
        const value = JSON.parse(response);
        setResult(Object.entries(value));
      })
      .catch(error => console.log('error', error));
  };

  return (
    <View style={styles.container}>
      <View>
        <Headerss
          navigation={navigation}
          title={'Riwayat Transaksi'}
          search={'Cari Transaksi'}
        />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.body}>
          {result.map((e, index) => {
            return (
              // kotak
              <View style={styles.list} key={index}>
                <View
                  style={{
                    flexDirection: 'column',
                  }}>
                  {/* judul&ket */}
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text style={styles.tipe}>{e[1].type}</Text>
                    <Text
                      style={{
                        color: '#00b2d6',
                        backgroundColor: '#b3d9ff',
                        borderRadius: 5,
                        fontWeight: 'bold',
                        padding: 5,
                        fontSize: 11,
                        marginBottom: 5,
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                      }}>
                      Berhasil
                    </Text>
                  </View>

                  {/* ROW */}
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    {/* gambar */}
                    <View
                      style={{
                        width: '10%',
                      }}>
                      <Image
                        source={icHistpay}
                        style={{
                          height: 40,
                          width: 40,
                          resizeMode: 'contain',
                          marginTop: 10,
                          borderRadius: 100,
                          borderColor: '#00B2D6',
                          borderWidth: 3,
                        }}
                      />
                    </View>
                    {/* data */}
                    <View
                      style={{
                        width: '85%',
                      }}>
                      <Text style={styles.data}>Pengirim : {e[0]}</Text>
                      <Text style={styles.data}>Penerima : {e[1].target}</Text>
                      <Text style={styles.data}>Jumlah : {e[1].amount}</Text>
                    </View>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000',
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
  tipe: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    // marginLeft: 10,
    width: 150,
  },
});

export default History;
