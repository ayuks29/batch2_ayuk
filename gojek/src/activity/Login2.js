import {View, Text, TextInput, TouchableOpacity, Button, Alert} from 'react-native';
import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const validateEmail = () => {
    if (email === '') {
      Alert.alert('Error', 'Email tidak boleh kosong');
      return false;
    }
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      Alert.alert('Error', 'Email tidak valid');
      return false;
    }
    return true;
  };

  const validatePassword = () => {
    if (password === '') {
      Alert.alert('Error', 'Password tidak boleh kosong');
      return false;
    }
    if (password.length < 8) {
      Alert.alert('Error', 'Password minimal 8 karakter');
      return false;
    }
    if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password)) {
      Alert.alert('Error', 'Password harus terdiri dari huruf besar, huruf kecil, dan angka');
      return false;
    }
    return true;
    
  };

  const getData = () => {
    const dataToken = async value => {
      await AsyncStorage.setItem('Token', value);
    };
    var data = JSON.stringify({
      name: 'test',
      phone: '083812838123',
      address: 'test',
      password: '123',
    });

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        dataToken(JSON.stringify(data));
        navigation.navigate('home2' || 'Home');
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  //   const DataUser = async () => {
  //     const token = await AsyncStorage.getItem('Token');
  //     console.log(token, 'data token');
  //   };
  //   useEffect(() => {
  //     DataUser();
  //   }, []);

  return (
    <View style={{flex: 1}}>
      <View>
        <TextInput
          placeholder="Email"
          placeholderTextColor={'black'}
          value={email}
          onChangeText={e => {
            setEmail(e);
          }}
        />
      </View>
      <View>
        <TextInput
          placeholder="Password"
          placeholderTextColor={'black'}
          value={password}
          onChangeText={e => {
            setPassword(e);
          }}
        />
      </View>
      <TouchableOpacity
        onPress={function () {
          getData();
        }}>
        <Text>Button log</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;
