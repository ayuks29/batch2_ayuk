import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Button,
  Alert,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const validateEmail = () => {
    if (email === '') {
      console.error('Error', 'Email tidak boleh kosong');
      return false;
    }
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      console.error('Error', 'Email tidak valid');
      return false;
    }
    return true;
  };

  const validatePassword = () => {
    if (password === '') {
      console.error('Error', 'Password tidak boleh kosong');
      return false;
    }
    if (password.length < 8) {
      console.error('Error', 'Password minimal 8 karakter');
      return false;
    }
    if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password)) {
      console.error(
        'Password harus terdiri dari huruf besar, huruf kecil, dan angka',
      );
      return false;
    }
    return true;
  };

  const handleSubmit = () => {
    if (validateEmail() && validatePassword()) {
      Alert.alert('Sukses!!', 'Berhasil Login', [
        {text: 'OK', onPress: () => navigation.navigate('home2')},
      ]);
      console.log('BERHASIL HOREE WE DID IT', email, password);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{flex: 1}}>
      <SafeAreaView style={styles.container}>
        {/* bisa dipakai bisa engga */}
        <ScrollView>
          <View>
            <View
              style={{
                alignItems: 'center',
                marginTop: 50,
                marginBottom: 20,
              }}>
              <Image
                source={{
                  uri: 'https://cdn-icons-png.flaticon.com/512/4961/4961705.png',
                }}
                style={{
                  width: 180,
                  height: 180,
                }}
              />
            </View>
            <Text
              style={{
                textAlign: 'right',
                marginRight: 20,
                marginBottom: 20,
                fontWeight: 'bold',
                fontSize: 16,
              }}>
              Welcome Back !
            </Text>
            <View>
              <TextInput
                placeholder="Email"
                placeholderTextColor={'black'}
                value={email}
                onChangeText={e => {
                  setEmail(e);
                }}
                style={styles.txtIn}
              />
            </View>
            <View>
              <TextInput
                placeholder="Password"
                placeholderTextColor={'black'}
                value={password}
                secureTextEntry
                onChangeText={e => {
                  setPassword(e);
                }}
                onBlur={validatePassword}
                style={styles.txtIn}
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                handleSubmit()
              }}
              style={styles.button}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 17,
                }}>
                Login
              </Text>
            </TouchableOpacity>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                alignContent: 'center',
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                }}>
                Don't have account?
              </Text>
              <TouchableOpacity>
                <Text>Register</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e6fff9',
  },
  txtIn: {
    borderWidth: 1,
    // borderColor: 'red',
    marginTop: -5,
    padding: 10,
    margin: 20,
    height: 40,
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: 'white',
    borderColor:'#00cca3'
  },
  button: {
    borderRadius: 18,
    margin: 20,
    backgroundColor: '#99e6e6',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Login;
