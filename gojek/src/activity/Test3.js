import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';

const MyComponent = () => {
    const [result, setResult] = useState([]);
    
    const getData = async () => {
        var raw = "";
    
        var requestOptions = {
        method: 'GET',
        body: raw,
        redirect: 'follow'
        }

        fetch("https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json", requestOptions)
          .then(response => response.text())
        //   .then(res => console.log(res))
          
            .then(response => {
                const value = JSON.parse(response)
                setResult(Object.entries(value))
            // console.log(response)
            // const data = Object.entries(response)
            // setResult(data)

          })
          .catch(error => console.log('error', error));
    }

    useEffect(() => {
        getData()
    }, []);
    
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.body}>
            {/*  */}
          {result.map((e, index) => {
            return (
              <View style={styles.list} key={index}>
                <Text style={styles.No}>NO : {index}</Text>
                <Text style={styles.data}>ID : {e[0]}</Text>
                <Text style={styles.data}>Tujuan : {e[1].target}</Text>
                <Text style={styles.data}>Tipe Transaksi : {e[1].type}</Text>
                <Text style={styles.data}>Biaya : {e[1].amount}</Text>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent:'center',
      alignItems:'center'
    },
    list: {
      marginVertical: 10,
      backgroundColor: 'white',
      borderRadius: 20,
      borderWidth: 2,
      borderColor: 'white',
      width: 300,
      justifyContent:'center',
      alignContent:'center'
    },
    No: {
      fontWeight: 'bold',
      color: 'black',
      fontFamily: 'Montserrat-Light',
    },
    data: {
      fontWeight: '600',
      color: 'black',
      fontFamily: 'Montserrat-Regular',
    },
    text: {
      fontFamily: 'Montserrat-Bold',
      fontSize: 25,
      color: 'black',
      marginHorizontal: 25,
      paddingBottom: 10,
    },
  });

export default MyComponent;
