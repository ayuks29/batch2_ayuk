import { View, Text, BackHandler } from 'react-native'
import React, { useEffect } from 'react'

const Gofood = ({navigation}) => {
    const HandlerBack = () => {
        navigation.goBack()
        return true
    }

    useEffect(() => {
      BackHandler.addEventListener('hardwareBackPress', HandlerBack)
    
      return () => {
        BackHandler.removeEventListener('hardwareBackPress', HandlerBack)
      }
    }, [])
    
  return (
    <View>
      <Text style={{fontWeight:'bold'}}>GoFood</Text>
    </View>
  )
}

export default Gofood