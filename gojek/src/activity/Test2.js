import { View, Text, ScrollView, TouchableOpacity, StyleSheet } from 'react-native'
import React, { useState } from 'react'

const ScButton = ({
    data
}) => {
    const [Change, setChange] = useState(0)
    const ChangeColor = (itemID) => {
        setChange(currentItem => currentItem === itemID ? itemID : itemID)
    }
    return (
        <View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {data.map((data, index) => {
                    const colors = Change == data.id ? '#fff' : '#004CE8'
                    return (
                        <TouchableOpacity
                            key={index}
                            onPress={() => {
                                ChangeColor(data.id)
                            }}>
                            <View
                                style={style.conData(Change, data.id)}>
                                <Text
                                    style={{
                                        color: colors
                                    }}>
                                    {data.label}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    )
                })}
            </ScrollView>
        </View>
    )
}

const style = StyleSheet.create({
    conData: (click, data) => ({
        backgroundColor: click !== data ? '#fff' : '#004CE8',
        borderRadius: 24,
        height: 34,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 16,
        paddingVertical: 6,
        marginRight: 8,
        borderWidth: 1,
        borderColor: '#004CE8'
    }),
})

export default ScButton