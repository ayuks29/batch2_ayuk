import {View, Text, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';

const home = ({navigation}) => {
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount(count + 1);
  };
  const decrem = () => {
    setCount(count - 1);
  };
  useEffect(() => {
    console.log(count);
  }, [count]);

  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <Text
        style={{
          alignSelf: 'center',
          color: 'black',
          fontWeight: 'bold',
          fontSize: 16,
        }}>
        {count}
      </Text>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => {
            increment();
          }}>
          <Text
            style={{
              color: 'black',
            }}>
            Button 1
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Class');
          }}>
          <Text>Ke Class</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            decrem();
          }}>
          <Text
            style={{
              color: 'black',
            }}>
            Button 2
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default home;
