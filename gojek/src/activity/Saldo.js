import {
  BackHandler,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {RadioButton, Text} from 'react-native-paper';
import Headerss from '../component/Headers2';
import {SafeAreaView} from 'react-native-safe-area-context';

const Saldo = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };
  const [disables, setDisables] = useState(false);
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');

  const ValidatorButton = () => {
    return dataAmount !== '' &&
      dataSender !== '' &&
      dataTarget !== '' &&
      dataType !== ''
      ? setDisables(false)
      : setDisables(true);
  };

  const showToast = () => {
    ToastAndroid.show("Transaksi Berhasil", ToastAndroid.SHORT);
  };

  useEffect(() => {
    ValidatorButton();
  }, [dataAmount, dataSender, dataTarget, dataType]);

  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  function createTrx() {
    var axios = require('axios');
        var data = JSON.stringify({
          "amount": dataAmount,
          "sender": dataSender,
          "target": dataTarget,
          "type": dataType
        });
        
        var config = {
          method: 'post',
        maxBodyLength: Infinity,
          url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
          headers: { 
            'Content-Type': 'application/json'
          },
          data : data
        };
        
        axios(config)
        .then(function (response) {
          console.log(JSON.stringify(response.data), "Berhasil Hore");
        })
        .catch(function (error) {
          console.log(error);
        });
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{backgroundColor:'white',flex: 1}}>
      <SafeAreaView>
        <ScrollView>
          <View>
            <Headerss title={'Transaksi'} navigation={navigation} />
            <View>
              <Image
                source={{
                  uri: 'https://img.icons8.com/fluency/512/transaction-approved.png',
                }}
                style={{
                  height: 150,
                  width: 150,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                  marginTop: 10,
                }}
              />
            </View>
            <View
              style={{
                borderWidth: 1,
                margin: 20,
                borderColor: '#d9d9d9',
                borderRadius: 8,
                backgroundColor:'#e2e6e9'
              }}>
              <Text
                style={{
                  marginTop: 20,
                  marginLeft: 20,
                }}>
                Pilih Jenis Transaksi
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                  margin: 3,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <RadioButton
                    value="kontak"
                    status={dataType === 'kontak' ? 'checked' : 'unchecked'}
                    onPress={() => setDataType('kontak')}
                    color="#00cc85"
                  />
                  <Text>Kirim Ke Kontak</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <RadioButton
                    value="bank"
                    status={dataType === 'bank' ? 'checked' : 'unchecked'}
                    onPress={() => setDataType('bank')}
                    color="#00cc85"
                  />
                  <Text>Kirim Ke Bank</Text>
                </View>
              </View>
              <TextInput
                style={styles.txt}
                placeholder="masukkan nominal"
                placeholderTextColor={'black'}
                keyboardType={'number-pad'}
                value={dataAmount}
                onChangeText={am => {
                  setDataAmount(rupiah(am));
                }}
              />

              <TextInput
                style={styles.txt}
                placeholder="masukkan nama pengirim"
                placeholderTextColor={'black'}
                value={dataSender}
                onChangeText={send => {
                  setDataSender(send);
                }}
              />

              <TextInput
                style={styles.txt1}
                placeholder="masukkan nama penerima"
                placeholderTextColor={'black'}
                value={dataTarget}
                onChangeText={tg => {
                  setDataTarget(tg);
                }}
              />
            </View>

            <View>
              <TouchableOpacity
                disabled={disables}
                onPress={() => {
                  createTrx();
                  navigation.navigate('Home')
                  showToast()
                }}
                style={{
                  backgroundColor: disables == false ? '#00c07d' : '#f0f3f4',
                  marginHorizontal: 20,
                  paddingVertical: 15,
                  paddingHorizontal: 20,
                  marginTop: 8,
                  borderRadius: 8,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: disables == false ? 'white' : 'grey',
                    fontSize: 18,
                    fontWeight: 'bold',
                  }}>
                  Kirim
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  txt: {
    height: 50,
    borderWidth: 1,
    borderRadius: 8,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    backgroundColor: 'white',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  txt1: {
    height: 50,
    borderWidth: 1,
    borderRadius: 8,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    backgroundColor: 'white',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 22,
  },
});
export default Saldo;
