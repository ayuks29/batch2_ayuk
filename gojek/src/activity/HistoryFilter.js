import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import icHistpay from '../../icons/historypay.png';
import AscDsc from '../component/AscDsc';

const History = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [text, settext] = useState('');

  useEffect(() => {
    getData();
  }, []);

  const [result, setResult] = useState([]);
  const getData = async () => {
    var raw = '';

    var requestOptions = {
      method: 'GET',
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(response => {
        const value = JSON.parse(response);
        setResult(Object.entries(value));
      })
      .catch(error => console.log('error', error));
  };

  return (
    <View style={styles.container}>
      {/* BEGIN OF HEADER */}
      <View style={styles.header}>
        <View style={styles.back}>
          <View>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Image
                source={{
                  uri: 'https://img.icons8.com/external-royyan-wijaya-detailed-outline-royyan-wijaya/512/external-arrow-arrow-line-royyan-wijaya-detailed-outline-royyan-wijaya-5.png',
                }}
                style={{
                  height: 23,
                  width: 40,
                  resizeMode: 'contain',
                }}
              />
            </TouchableOpacity>
          </View>
          <View>
            <Text style={styles.text}>Riwayat Transaksi</Text>
          </View>
        </View>
        {/* BREAK */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 5,
          }}>
          <View style={styles.cari}>
            <View>
              <Image
                source={{
                  uri: 'https://img.icons8.com/ios-filled/512/search--v1.png',
                }}
                style={{height: 23, width: 40, resizeMode: 'contain'}}
              />
            </View>
            <TextInput
              style={{textAlign: 'left', color: '#333333'}}
              onChangeText={text => settext(text)}
              value={text}
              placeholder="masukkan nama atau tipe transaksi"
            />
          </View>
          <AscDsc/>
        </View>
      </View>
      {/* END OF HEADER */}
      <ScrollView showsVerticalScrollIndicator={false}>
        {result.map((e, index) => {
          const filter = e.filter(item => {
            return (
              item?.type?.toLowerCase().includes(text?.toLowerCase()) ||
              item?.sender
                ?.toString()
                .toLowerCase()
                .includes(text?.toLowerCase()) ||
              item?.target
                ?.toString()
                .toLowerCase()
                .includes(text?.toLowerCase())
            );
          });

          return (
            <View style={styles.body}>
              {filter.map((es, index) => {
                return (
                  // kotak
                  <View style={styles.list} key={index}>
                    <View
                      style={{
                        flexDirection: 'column',
                      }}>
                      {/* judul&ket */}
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={styles.tipe}>{es.type}</Text>
                        <Text
                          style={{
                            color: '#00b2d6',
                            backgroundColor: '#b3d9ff',
                            borderRadius: 5,
                            fontWeight: 'bold',
                            padding: 5,
                            fontSize: 11,
                            marginBottom: 5,
                            justifyContent: 'flex-end',
                            alignItems: 'flex-end',
                          }}>
                          Berhasil
                        </Text>
                      </View>

                      {/* ROW */}
                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        {/* gambar */}
                        <View
                          style={{
                            width: '10%',
                          }}>
                          <Image
                            source={icHistpay}
                            style={{
                              height: 40,
                              width: 40,
                              resizeMode: 'contain',
                              marginTop: 10,
                              borderRadius: 100,
                              borderColor: '#00B2D6',
                              borderWidth: 3,
                            }}
                          />
                        </View>
                        {/* data */}
                        <View
                          style={{
                            width: '85%',
                          }}>
                          <Text style={styles.data}>
                            Pengirim : {es.sender}
                          </Text>
                          <Text style={styles.data}>
                            Penerima : {es.target}
                          </Text>
                          <Text style={styles.data}>Jumlah : {es.amount}</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                );
              })}
            </View>
          );
        })}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000',
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
  tipe: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    // marginLeft: 10,
    width: 150,
  },
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginBottom: 3,
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
  cari: {
    backgroundColor: '#f7f7f8',
    borderWidth: 1,
    borderRadius: 16,
    borderColor: '#d9d9d9',
    height: 40,
    width: '82%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 5,
  },
});

export default History;
