import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import Headerss from '../component/Headers2';
import React, {useState, useEffect} from 'react';
import icHistpay from '../../icons/historypay.png';

const History = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  useEffect(() => {
    getData();
  }, []);

  const [result, setResult] = useState([]);
  const [ketikan, setKetikan] = useState('');
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [screens, setScreens] = useState(1);
  const getData = async () => {
    setLoading(true);
    var raw = '';

    var requestOptions = {
      method: 'GET',
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(res => {
        return res.text();
      })
      .then(response => {
        const value = JSON.parse(response);
        setResult(Object.entries(value));
        dataASCDSC(value);
        setLoading(false);
      })
      .catch(error => {
        setLoading(false);
        console.log('error', error);
      });
  };
  const dataASCDSC = async data => {
    const dataArray = Object.keys(data).map(key => ({
      sender: data[key].sender,
      target: data[key].target,
      amount: data[key].amount,
      type: data[key].type,
    }));
    setData(dataArray);
  };

  const sortAscending = () => {
    const sortedData = [...data].sort((a, b) => a.amount - b.amount);
    setData(sortedData);
  };

  const sortDescending = () => {
    const sortedData = [...data].sort((a, b) => b.amount - a.amount);
    setData(sortedData);
  };

  if (loading === true) {
    return <Text>Loading...</Text>;
  }
  return (
    <View style={styles.container}>
      <View>
        <Headerss
          navigation={navigation}
          title={'Riwayat Transaksi'}
          KetikanUser={ketikan}
          UbahKetikanUser={e => {
            setKetikan(e);
            setScreens(1);
          }}
          onPress2={() => {
            sortAscending();
            setScreens(2);
          }}
          onPress1={() => {
            sortDescending();
            setScreens(2);
          }}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
      </View>
      <View>
        {screens == 1 ? (
          <ScrollView>
            {result?.map((e, index) => {
              const filter = e.filter(item => {
                return (
                  item?.type?.toLowerCase().includes(ketikan?.toLowerCase()) ||
                  item?.sender
                    ?.toString()
                    .toLowerCase()
                    .includes(ketikan?.toLowerCase()) ||
                  item?.target
                    ?.toString()
                    .toLowerCase()
                    .includes(ketikan?.toLowerCase())
                );
              });

              return (
                <View style={styles.body}>
                  {filter.map((es, index) => {
                    return (
                      // kotak
                      <View style={styles.list} key={index}>
                        <View
                          style={{
                            flexDirection: 'column',
                          }}>
                          {/* judul&ket */}
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            {/* <Text style={styles.tipe}>{e[0]}</Text> */}
                            <Text style={styles.tipe}>{es.type}</Text>

                            <Text
                              style={{
                                color: '#00b2d6',
                                backgroundColor: '#b3d9ff',
                                borderRadius: 5,
                                fontWeight: 'bold',
                                padding: 5,
                                fontSize: 11,
                                marginBottom: 5,
                                justifyContent: 'flex-end',
                                alignItems: 'flex-end',
                              }}>
                              Berhasil
                            </Text>
                          </View>

                          {/* ROW */}
                          <View
                            style={{
                              flexDirection: 'row',
                            }}>
                            {/* gambar */}
                            <View
                              style={{
                                width: '10%',
                              }}>
                              <Image
                                source={icHistpay}
                                style={{
                                  height: 40,
                                  width: 40,
                                  resizeMode: 'contain',
                                  marginTop: 10,
                                  borderRadius: 100,
                                  borderColor: '#00B2D6',
                                  borderWidth: 3,
                                }}
                              />
                            </View>
                            {/* data */}
                            <View
                              style={{
                                width: '85%',
                              }}>
                              <Text style={styles.data}>
                                Pengirim : {es.sender}
                              </Text>
                              <Text style={styles.data}>
                                Penerima : {es.target}
                              </Text>
                              <Text style={styles.data}>
                                Jumlah : {es.amount}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    );
                  })}
                </View>
              );
            })}
          </ScrollView>
        ) : (
          <ScrollView>
            {data.length > 0 ? (
              data.map((es, index) => (
                <View style={styles.list} key={index}>
                  <View
                    style={{
                      flexDirection: 'column',
                    }}>
                    {/* judul&ket */}
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={styles.tipe}>{es.type}</Text>
                      <Text
                        style={{
                          color: '#00b2d6',
                          backgroundColor: '#b3d9ff',
                          borderRadius: 5,
                          fontWeight: 'bold',
                          padding: 5,
                          fontSize: 11,
                          marginBottom: 5,
                          justifyContent: 'flex-end',
                          alignItems: 'flex-end',
                        }}>
                        Berhasil
                      </Text>
                    </View>

                    {/* ROW */}
                    <View
                      style={{
                        flexDirection: 'row',
                      }}>
                      {/* gambar */}
                      <View
                        style={{
                          width: '10%',
                        }}>
                        <Image
                          source={icHistpay}
                          style={{
                            height: 40,
                            width: 40,
                            resizeMode: 'contain',
                            marginTop: 10,
                            borderRadius: 100,
                            borderColor: '#00B2D6',
                            borderWidth: 3,
                          }}
                        />
                      </View>
                      {/* data */}
                      <View
                        style={{
                          width: '85%',
                        }}>
                        <Text style={styles.data}>Pengirim : {es.sender}</Text>
                        <Text style={styles.data}>Penerima : {es.target}</Text>
                        <Text style={styles.data}>Jumlah : {es.amount}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              ))
            ) : (
              <Text></Text>
            )}
          </ScrollView>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'white',
  },
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000',
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
  tipe: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    // marginLeft: 10,
    width: 150,
  },
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginBottom: 3,
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
});

export default History;
