import React, {useEffect, useState} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../activity/Home';
import Goride from '../activity/Goride';
import Gocar from '../activity/Gocar';
import Gofood from '../activity/Gofood';
import Goblue from '../activity/Goblue';
import Gosend from '../activity/Gosend';
import HistoryFilter from '../activity/HistoryFilter';
import History2 from '../activity/History2';
import Saldo from '../activity/Saldo';
import AscDsc from '../component/AscDsc';
import Login from '../activity/Login';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createNativeStackNavigator();

function Route() {
  const [tokens, settokens] = useState('');
  const DataUser = async () => {
    const tokens = await AsyncStorage.getItem('Token');
    settokens(tokens);
  };
  useEffect(() => {
    DataUser();
  }, []);
  console.log('route', tokens);
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {tokens == null ? (
        <Stack.Screen name="Login" component={Login} />
      ) : (
        <Stack.Screen name="Home" component={Home} />
      )}
      <Stack.Screen name="Login2" component={Login} />
      <Stack.Screen name="home2" component={Home} />
      <Stack.Screen name="Goride" component={Goride} />
      <Stack.Screen name="Gocar" component={Gocar} />
      <Stack.Screen name="Gofood" component={Gofood} />
      <Stack.Screen name="Goblue" component={Goblue} />
      <Stack.Screen name="Gosend" component={Gosend} />
      <Stack.Screen name="History2" component={History2} />
      <Stack.Screen name="AscDc" component={AscDsc} />
      <Stack.Screen name="Saldo" component={Saldo} />
    </Stack.Navigator>
  );
}

export default Route;
