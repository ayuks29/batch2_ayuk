import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const Headerss = ({title, search, navigation}) => {
  return (
    <View style={styles.header}>
      <View style={styles.back}>
        <View>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image
              source={{
                uri: 'https://img.icons8.com/external-royyan-wijaya-detailed-outline-royyan-wijaya/512/external-arrow-arrow-line-royyan-wijaya-detailed-outline-royyan-wijaya-5.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.text}>{title}</Text>
        </View>
      </View>
      {/* BREAK */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 5,
        }}>
        <View style={styles.cari}>
          <View>
            <Image
              source={{
                uri: 'https://img.icons8.com/ios-filled/512/search--v1.png',
              }}
              style={{height: 23, width: 40, resizeMode: 'contain'}}
            />
          </View>
          <Text style={{textAlign: 'left', color: '#333333'}}>{search}</Text>
        </View>
        <View
          style={{
            width: '5%',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
          }}>
          <Image
            source={{
              uri: 'https://img.icons8.com/ios/512/empty-filter.png',
            }}
            style={{height: 23, width: 40, resizeMode: 'contain', marginTop: 5}}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginBottom: 3,
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
  cari: {
    backgroundColor: '#f7f7f8',
    borderWidth: 1,
    borderRadius: 16,
    borderColor: '#d9d9d9',
    height: 40,
    width: '88%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 5,
  },
});

export default Headerss;
