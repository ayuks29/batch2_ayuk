import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  ScrollView,
  BackHandler,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import icHistpay from '../../icons/historypay.png';

const Search = ({navigation, KetikanUser, UbahKetikanUser}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);
  const [text, settext] = useState('');

  return (
    <View style={styles.container}>
      <View style={styles.cari}>
        <View>
          <Image
            source={{
              uri: 'https://img.icons8.com/ios-filled/512/search--v1.png',
            }}
            style={{height: 23, width: 40, resizeMode: 'contain'}}
          />
        </View>
        <TextInput
          style={{textAlign: 'left', color: '#333333'}}
          onChangeText={text => {
            settext(text);
            UbahKetikanUser(text);
          }}
          value={KetikanUser}
          placeholder="masukkan nama atau tipe transaksi"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor: 'gray',
  },
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#000',
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
  tipe: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
    // marginLeft: 10,
    width: 150,
  },
  header: {
    padding: 10,
    backgroundColor: 'white',
    elevation: 5,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginBottom: 3,
  },
  back: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginBottom: 5,
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'black',
  },
  cari: {
    backgroundColor: '#f7f7f8',
    borderWidth: 1,
    borderRadius: 16,
    borderColor: '#d9d9d9',
    height: 40,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 5,
  },
});

export default Search;
