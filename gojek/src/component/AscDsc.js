import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import icHistpay from '../../icons/historypay.png';
// import { Image } from 'react-native-paper/lib/typescript/components/Avatar/Avatar';

const AscDsc = ({onPress1, onPress2}) => {
  const [data, setData] = useState([]);

  // useEffect(() => {
  //   // Ambil data dari API menggunakan metode GET
  //   fetch(
  //     'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
  //   )
  //     .then(response => response.json())
  //     .then(data => {
  //       const dataArray = Object.keys(data).map(key => ({
  //         sender: data[key].sender,
  //         target: data[key].target,
  //         amount: data[key].amount,
  //         type: data[key].type,
  //       }));
  //       setData(dataArray);
  //     })
  //     .catch(error => {
  //       console.error('Error:', error);
  //       setData([]);
  //     });
  // }, []);

  // const sortAscending = () => {
  //   const sortedData = [...data].sort((a, b) => a.amount - b.amount);
  //   setData(sortedData);
  // };

  // const sortDescending = () => {
  //   const sortedData = [...data].sort((a, b) => b.amount - a.amount);
  //   setData(sortedData);
  // };

  // if (data.length === 0) {
  //   return <Text>Loading...</Text>;
  // }

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
        }}>
        <TouchableOpacity onPress={onPress1}>
          <View
            style={{
              width: '5%',
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
            }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/pastel-glyph/512/sort-amount-up.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
                marginTop: 5,
                marginRight: -6,
              }}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={onPress2}>
          <View
            style={{
              width: '5%',
              alignItems: 'flex-start',
              justifyContent: 'flex-start',
            }}>
            <Image
              source={{
                uri: 'https://img.icons8.com/pastel-glyph/512/generic-sorting.png',
              }}
              style={{
                height: 23,
                width: 40,
                resizeMode: 'contain',
                marginTop: 5,
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View></View>
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    justifyContent: 'center',
    alignContent: 'center',
    padding: 8,
    elevation: 1.5,
    borderRadius: 5,
    backgroundColor: 'white',
    marginTop: 8,
  },
  data: {
    color: 'black',
    fontFamily: 'Montserrat-Regular',
    marginLeft: 10,
  },
});

export default AscDsc;
